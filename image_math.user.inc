<?php

/**
 * @file
 * Functions for the generation of the CAPTCHA image.
 *
 * Loosely Based on MyCaptcha by Heine Deelstra
 * (http://heine.familiedeelstra.com/mycaptcha-download)
 */

/**
 * Menu callback function that generates the CAPTCHA image.
 */
function image_math_image($captcha_sid) {
  // If output buffering is on: discard current content and disable further buffering.
  if (ob_get_level()) {
    ob_end_clean();
  }

  // Get the question to display as our captcha
  $question = db_query('SELECT * FROM {captcha_image_math} WHERE sid = :sid', array(':sid' => $captcha_sid))->fetchObject();

  // Only generate captcha if code exists in the session.
  if ($question !== FALSE) {
    // Generate the image.
    $image = @_image_math_generate_image($question->question_text);
    // Make sure the creation of the image was a success.
    if (!$image) {
      watchdog('CAPTCHA', 'Generation of image CAPTCHA failed. Check your image CAPTCHA configuration and especially the used font.', array(), WATCHDOG_ERROR);
      exit();
    }
    // Send the image resource as an image file to the client.
    $file_format = variable_get('image_math_file_format', IMAGE_MATH_FILE_FORMAT_JPG);
    if ($file_format == IMAGE_MATH_FILE_FORMAT_JPG) {
      drupal_add_http_header('Content-Type', 'image/jpeg');
      imagejpeg($image);
    }
    else {
      drupal_add_http_header('Content-Type', 'image/png');
      imagepng($image);
    }
    // Clean up the image resource.
    imagedestroy($image);
  }
  exit();
}

/**
 * Small helper function for parsing a hexadecimal color to a RGB tuple.
 */
function _image_math_hex_to_rgb($hex) {
  // Handle #RGB format/
  if (strlen($hex) == 4) {
    $hex = $hex[1] . $hex[1] . $hex[2] . $hex[2] . $hex[3] . $hex[3];
  }
  $c = hexdec($hex);
  $rgb = array();
  for ($i = 16; $i >= 0; $i -= 8) {
    $rgb[] = ($c >> $i) & 0xFF;
  }
  return $rgb;
}

/**
 * Base function for generating a image CAPTCHA.
 */
function _image_math_generate_image($question_text = NULL) {
  if (empty($question_text)) {
    $question_text = variable_get('image_math_question_text', t('Solve this simple math problem and enter the result.'));
  }

  // Get font.
  $fonts = _image_math_get_enabled_font();
  
  // Get other settings.
  $font_size = (int) variable_get('image_math_font_size', 12);
  $dimensions = _image_math_image_size($question_text);
  $width = $dimensions[0];
  $height = $dimensions[1];
  
  // Create image resource.
  $image = imagecreatetruecolor($width, $height);
  if (!$image) {
    return FALSE;
  }

  // Get the background color and paint the background.
  $background_rgb = _image_math_hex_to_rgb(variable_get('image_math_background_color', '#FFFFFF'));
  $background_color = imagecolorallocate($image, $background_rgb[0], $background_rgb[1], $background_rgb[2]);
  // Set transparency if needed.
  $file_format = variable_get('image_math_file_format', IMAGE_MATH_FILE_FORMAT_JPG);
  if ($file_format == IMAGE_MATH_FILE_FORMAT_TRANSPARENT_PNG) {
    imagecolortransparent($image, $background_color);
  }
  imagefilledrectangle($image, 0, 0, $width, $height, $background_color);

  // Do we need to draw in RTL mode?
  global $language;
  $rtl = $language->direction && ((bool) variable_get('image_math_rtl_support', 0));

  // Draw text.
  $result = _image_math_image_generator_print_string($image, $width, $height, $fonts, $font_size, $question_text, $rtl);
  if (!$result) {
    return FALSE;
  }

  // Add noise.
  $noise_colors = array();
  for ($i = 0; $i < 20; $i++) {
    $noise_colors[] = imagecolorallocate($image, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
  }
  // Add additional noise.
  if (variable_get('image_math_dot_noise', 0)) {
    _image_math_image_generator_add_dots($image, $width, $height, $noise_colors);
  }
  if (variable_get('image_math_line_noise', 0)) {
    _image_math_image_generator_add_lines($image, $width, $height, $noise_colors);
  }

  // Distort the image.
  $distortion_amplitude = .25 * $font_size * variable_get('image_math_distortion_amplitude', 0) / 10.0;

  if ($distortion_amplitude > 1) {
    // Distortion parameters.
    $wavelength_xr = (2 + 3 * mt_rand(0, 1000) / 1000) * $font_size;
    $wavelength_yr = (2 + 3 * mt_rand(0, 1000) / 1000) * $font_size;
    $freq_xr = 2 * 3.141592 / $wavelength_xr;
    $freq_yr = 2 * 3.141592 / $wavelength_yr;
    $wavelength_xt = (2 + 3 * mt_rand(0, 1000) / 1000) * $font_size;
    $wavelength_yt = (2 + 3 * mt_rand(0, 1000) / 1000) * $font_size;
    $freq_xt = 2 * 3.141592 / $wavelength_xt;
    $freq_yt = 2 * 3.141592 / $wavelength_yt;

    $distorted_image = imagecreatetruecolor($width, $height);
    if ($file_format == IMAGE_MATH_FILE_FORMAT_TRANSPARENT_PNG) {
      imagecolortransparent($distorted_image, $background_color);
    }
    if (!$distorted_image) {
      return FALSE;
    }

    if (variable_get('image_math_bilinear_interpolation', FALSE)) {
      // Distortion with bilinear interpolation.
      for ($x = 0; $x < $width; $x++) {
        for ($y = 0; $y < $height; $y++) {
          // Get distorted sample point in source image.
          $r = $distortion_amplitude * sin($x * $freq_xr + $y * $freq_yr);
          $theta = $x * $freq_xt + $y * $freq_yt;
          $sx = $x + $r * cos($theta);
          $sy = $y + $r * sin($theta);
          $sxf = (int) floor($sx);
          $syf = (int) floor($sy);
          if ($sxf < 0 || $syf < 0 || $sxf >= $width - 1 || $syf >= $height - 1) {
            $color = $background_color;
          }
          else {
            // Bilinear interpolation: sample at four corners.
            $color_00 = imagecolorat($image, $sxf, $syf);
            $color_00_r = ($color_00 >> 16) & 0xFF;
            $color_00_g = ($color_00 >> 8) & 0xFF;
            $color_00_b = $color_00 & 0xFF;
            $color_10 = imagecolorat($image, $sxf + 1, $syf);
            $color_10_r = ($color_10 >> 16) & 0xFF;
            $color_10_g = ($color_10 >> 8) & 0xFF;
            $color_10_b = $color_10 & 0xFF;
            $color_01 = imagecolorat($image, $sxf, $syf + 1);
            $color_01_r = ($color_01 >> 16) & 0xFF;
            $color_01_g = ($color_01 >> 8) & 0xFF;
            $color_01_b = $color_01 & 0xFF;
            $color_11 = imagecolorat($image, $sxf + 1, $syf + 1);
            $color_11_r = ($color_11 >> 16) & 0xFF;
            $color_11_g = ($color_11 >> 8) & 0xFF;
            $color_11_b = $color_11 & 0xFF;
            // Interpolation factors.
            $u  = $sx - $sxf;
            $v  = $sy - $syf;
            // Interpolate.
            $r = (int) ((1 - $v) * ((1 - $u) * $color_00_r + $u * $color_10_r) + $v * ((1 - $u) * $color_01_r + $u * $color_11_r));
            $g = (int) ((1 - $v) * ((1 - $u) * $color_00_g + $u * $color_10_g) + $v * ((1 - $u) * $color_01_g + $u * $color_11_g));
            $b = (int) ((1 - $v) * ((1 - $u) * $color_00_b + $u * $color_10_b) + $v * ((1 - $u) * $color_01_b + $u * $color_11_b));
            // Build color.
            $color = ($r<<16) + ($g<<8) + $b;
          }
          imagesetpixel($distorted_image, $x, $y, $color);
        }
      }
    }
    else {
      // Distortion with nearest neighbor interpolation.
      for ($x = 0; $x < $width; $x++) {
        for ($y = 0; $y < $height; $y++) {
          // Get distorted sample point in source image.
          $r = $distortion_amplitude * sin($x * $freq_xr + $y * $freq_yr);
          $theta = $x * $freq_xt + $y * $freq_yt;
          $sx = $x + $r * cos($theta);
          $sy = $y + $r * sin($theta);
          $sxf = (int) floor($sx);
          $syf = (int) floor($sy);
          if ($sxf < 0 || $syf < 0 || $sxf >= $width - 1 || $syf >= $height - 1) {
            $color = $background_color;
          }
          else {
            $color = imagecolorat($image, $sxf, $syf);
          }
          imagesetpixel($distorted_image, $x, $y, $color);
        }
      }
    }
    // Release undistorted image.
    imagedestroy($image);
    // Return distorted image.
    return $distorted_image;
  }
  else {
    return $image;
  }
}

/**
 * Add lines.
 */
function _image_math_image_generator_add_lines(&$image, $width, $height, $colors) {
  $line_quantity = $width * $height / 200.0 * ((int) variable_get('image_math_noise_level', 5)) / 20.0;
  for ($i = 0; $i < $line_quantity; $i++) {
    imageline($image, mt_rand(0, $width), mt_rand(0, $height), mt_rand(0, $width), mt_rand(0, $height), $colors[array_rand($colors)]);
  }
}

/**
 * Add dots.
 */
function _image_math_image_generator_add_dots(&$image, $width, $height, $colors) {
  $noise_quantity = $width * $height * ((int) variable_get('image_math_noise_level', 5)) / 20.0;
  for ($i = 0; $i < $noise_quantity; $i++) {
    imagesetpixel($image, mt_rand(0, $width), mt_rand(0, $height), $colors[array_rand($colors)]);
  }
}

/**
 * Helper function for drawing text on the image.
 */
function _image_math_image_generator_print_string(&$image, $width, $height, $font, $font_size, $text, $rtl = FALSE) {
  // Get colors.
  $background_rgb = _image_math_hex_to_rgb(variable_get('image_math_background_color', '#ffffff'));
  $foreground_rgb = _image_math_hex_to_rgb(variable_get('image_math_foreground_color', '#000000'));
  $background_color = imagecolorallocate($image, $background_rgb[0], $background_rgb[1], $background_rgb[2]);
  $foreground_color = imagecolorallocate($image, $foreground_rgb[0], $foreground_rgb[1], $foreground_rgb[2]);
  
  // Set default text color.
  $color = $foreground_color;

  $pos_y = 0.5 * $height + 6;
  
  if ($font == 'BUILTIN') {
    imagestring($image, 5, 5, $pos_y, $text, $color);
  }
  else {
    imagettftext($image, $font_size, 0, 5, $pos_y, $color, drupal_realpath($font), utf8_encode($text));
  }
  return TRUE;
}
